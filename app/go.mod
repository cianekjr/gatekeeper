module gatekeeper

go 1.13

require (
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/graphql-go/graphql v0.7.8 // indirect
	github.com/rakyll/statik v0.1.6 // indirect
	github.com/samsarahq/go v0.0.0-20191115010820-ad30142aad11 // indirect
	github.com/samsarahq/thunder v0.5.0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
